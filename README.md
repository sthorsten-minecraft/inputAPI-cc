# Input API

## Description

This is my Input API for ComputerCraft. It allows for easier handling of user input.

## Features

This API contains the following features (functions):

- formatNumber() - Allows for integer values to be formatted (e.g. 1000 -> 1,000),
The seperator can be adjusted!

- getEnter() - Prints some text to the user and waits until *Enter* is pressed.

- yesNoInput() - Prints a question text to the user and gets the users feedback (e.g. *yes* or *no*),
The yes and no options can be adjusted!

- stringInput() - Prints a message to the user and gets a string the user inputs

- numberInput() - Asks the user to enter an integer between specific values.

## License

The MIT License

See the [LICENSE](LICENSE) file

© 2016 - 2018 Thor_s_Crafter / Thorsten Schmitt

## 


